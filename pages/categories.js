import { useState, useEffect } from 'react'
import Head from 'next/head'
import { Row, Col } from 'react-bootstrap'
import { Table, Modal, Form, Card } from 'react-bootstrap'
import { Alert, Button, ButtonGroup } from 'react-bootstrap'
import Banner from '../components/Banner'
import Swal from 'sweetalert2'
import Router from 'next/router'


export default function Categories(){

	const [ categoryDetails, setCategoryDetails ] = useState([])
	const [ categoryStorage, setCategoryStorage ] = useState([])

	const [ catName, setCatName ] = useState('')
	const [ catType, setCatType ] = useState('')
	const [ show, setShow ] = useState(false)

	const [ showEdit, setShowEdit ] = useState(false)
	const [ nameEdit, setNameEdit ] = useState('')
	const [ typeEdit, setTypeEdit ] = useState('')
	const [ idEdit, setIdEdit ] = useState('')
	const [ hasNoBlank, setHasNoBlank ] = useState(false)

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {headers:{'Authorization':`Bearer ${localStorage.getItem('token')}`}})
		.then(res => res.json())
		.then(data => {
			setCategoryDetails(data)
			setCategoryStorage(data)
		})
	}, [hasNoBlank, show, showEdit])

	useEffect(() => {
		if(catName !== '' && catType !== 'Choose'){
			setHasNoBlank(true)
		} else {
			setHasNoBlank(false)
		}
	}, [catName, catType])

	const categoryItems = categoryDetails.map(category => {
		return(
			<tr key={category._id}>
				<td>{category.name}</td>
				<td>{category.type}</td>
				<td><Button variant='outline-warning' className='btn-block' onClick={() => {handleShowEdit(category._id)}}>Edit</Button></td>
			</tr>
			)
	})

	function handleShow(){
		setShow(true)
	}

	function handleClose(){
		setShow(false)
		setCatName('')
		setCatType('')
	}

	function handleShowEdit(catId){
		setIdEdit(catId)
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories/${catId}`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setNameEdit(data[0].name)
			setTypeEdit(data[0].type)
			setShowEdit(true)
		})
		

	}

	function handleCloseEdit(){
		setShowEdit(false)
		setNameEdit('')
		setTypeEdit('')
		setIdEdit('')
	}

	function addCategory(e){
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: catName,
				type: catType
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({icon:'success',title:'New Category Added'})
				setCatName('')
				setCatType('')
				handleClose()
			} else {
				Swal.fire({icon:'error',title:'Something went wrong...'})
			} 
		})
	}

	function editCategory(e){
		e.preventDefault()
		Swal.fire({
			title: 'Are you sure?',
			showDenyButton: true,
			confirmButtonText:'Save Edit',
			denyButtonText: 'Cancel',
			confirmButtonColor: '#d33',
			denyButtonColor: '#3085d6'
		})
		.then(result => {
			if(result.isConfirmed){
				Swal.fire('Saving...')
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories/${idEdit}`, {
					method: 'PATCH',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name: nameEdit,
						type: typeEdit,
						categoryId: idEdit
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data){
						Swal.fire({icon:'success',title:'Edit Successful'})
						handleCloseEdit()
					} else {
						Swal.fire({icon:'error',title:'Something went wrong...'})
					}
				})
			} else if (result.isDenied){
				Swal.fire('Cancelled')
			}
		})
	}

	function deleteCategory(e){
		e.preventDefault()
		Swal.fire({
			title: 'Are you sure?',
			showDenyButton: true,
			confirmButtonText:'Delete Category',
			denyButtonText: 'Cancel',
			confirmButtonColor: '#d33',
			denyButtonColor: '#3085d6'
		})
		.then(result => {
			if(result.isConfirmed){
				Swal.fire('Deleting')
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories/${idEdit}`,{
					method: 'DELETE',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						categoryId: idEdit
					})
				})
				.then(res => res.json())
				.then(data => {
					data === true ? Swal.fire('Deleted') : Swal.fire('Error occured')
				})
			} else if (result.isDenied){
				Swal.fire('Cancelled')
			}
		})
	}


	return(
		<>
			<Head>
				<title>Categories || Budget Tracker</title>
			</Head>
			<Banner data='Categories'/>
			<Card>
				<Card.Body>
					<Card.Text>Total Categories: {categoryStorage.length}</Card.Text>
					<Card.Text>Income Categories: {categoryStorage.filter(cat=> cat.type==='Income').length}</Card.Text>
					<Card.Text>Expense Categories: {categoryStorage.filter(cat=> cat.type==='Expense').length}</Card.Text>
				</Card.Body>
				<Button variant='success' className='my-3 btn-block' onClick={handleShow}>Add New Category</Button>
			</Card>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>Category</th>
						<th>Type</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					{
						categoryItems
					}
				</tbody>
			</Table>
			<Modal show={show} onHide={handleClose}>
				<Modal.Header>
					<Modal.Title>Add Category</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => addCategory(e)}>
						<Form.Group controlId='categoryName'>
							<Form.Label>Category:</Form.Label>
							<Form.Control type='text' value={catName} onChange={e => setCatName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId='categoryType'>
							<Form.Label>Type:</Form.Label>
							<Form.Control as='select' value={catType} onChange={e => setCatType(e.target.value)} required>
								<option value="Choose">Choose type...</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</Form.Control>
						</Form.Group>
						{
							hasNoBlank
							?	<Button type='submit' variant='outline-dark'>Add Category</Button>
							: 	<Button variant='outline-secondary' disabled>Add Category</Button>
						}
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='dark' onClick={handleClose}>&times;Close</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={showEdit} onHide={handleCloseEdit}>
				<Modal.Header>
					<Modal.Title>Edit Category</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => editCategory(e)}>
						<Form.Group controlId='newName'>
							<Form.Label>New Category Name:</Form.Label>
							<Form.Control type='text' value={nameEdit} onChange={e=>setNameEdit(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId='newType'>
							<Form.Control as='select' value={typeEdit} onChange={e=>setTypeEdit(e.target.value)} required>
						     	<option value="Income">Income</option>
							    <option value="Expense">Expense</option>
							</Form.Control>
						</Form.Group>
						<ButtonGroup>
							<Button type='submit' variant='warning'>Save Edit</Button> 
							<Button variant='secondary'>↔</Button> 
							<Button variant='danger' onClick={e => deleteCategory(e)}>Delete</Button> 
						</ButtonGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='dark' onClick={handleCloseEdit}>Close</Button>
				</Modal.Footer>
			</Modal>
		</>
		)
}