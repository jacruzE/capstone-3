import { useEffect, useState } from 'react'
import moment from 'moment'
import Head from 'next/head'
import Router from 'next/router'
import { Form, Row, Col } from 'react-bootstrap'

import TrendChart from '../components/TrendChart'

export default function Trends(){
	console.log(new moment().format('YYYY-MM-DD'))
	const [ base, setBase ] = useState('')
	const [ range, setRange ] = useState('')

	const [ rawData, setRawData ] = useState([])
	const [ dates, setDates ] = useState([])
	const [ values, setValues ] = useState([])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`,{
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setRawData(data.map(data=>data))
		})
		setTimeout(() => {
			setBase(new moment().date(1).format('YYYY-MM-DD'))
			setRange(new moment().date(0).add(1,'months').format('YYYY-MM-DD'))
		}, 500)
	}, [])

	useEffect(() => {
		let temp = []
		let tempDates = []
		rawData.sort((a,b) => moment(a.transaction.date) - moment(b.transaction.date))
		rawData.forEach(data => {
			if(moment(data.transaction.date).isSameOrAfter(base) && moment(data.transaction.date).isSameOrBefore(range) ){
				temp.push(data.transaction.resultingBalance)
				tempDates.push(`${moment(data.transaction.date).date()+1}/${moment(data.transaction.date).month()+1}`)
			}
		})
		setValues(temp)
		setDates(tempDates)
	}, [range, base])


	return(
		<>
			<Head>
				<title>Trends | Budget Tracker</title>
			</Head>
			<h1 className="my-3">Balance Trend</h1>
			<Form className='d-flex'>
				<Form.Group as={Col} sm={6}>
					<Form.Label>From:</Form.Label>
					<Form.Control type='date' value={base} onChange={e => setBase(e.target.value)}/>
				</Form.Group>
				<Form.Group as={Col} sm={6}>
					<Form.Label>To:</Form.Label>
					<Form.Control type='date' value={range} onChange={e => setRange(e.target.value)}/>
				</Form.Group>
			</Form>
			<TrendChart values={values} dates={dates}/>
		</>
		)
}