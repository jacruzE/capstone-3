import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'

import { useState, useEffect, Fragment } from 'react'
import { Container } from 'react-bootstrap'

import { UserProvider } from '../UserContext'
import NavBar from '../components/NavBar' 

function MyApp({ Component, pageProps }) {

	const [ user, setUser ] = useState({id:null})	

	useEffect(() => {
			fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/details`, {
				headers: {
					'Authorization' : `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				data._id ? setUser({id:data._id,email:data.email}) : setUser({id:null,email:null})
			})
	}, [user.id])

	const unsetUser = () => {
		localStorage.clear()
		setUser({id:null,email:null})
	}


  return (
  	<Fragment>
  		<UserProvider value={{user, setUser, unsetUser}}>
  			<NavBar />
  			<Container>
  				<Component {...pageProps} />
  			</Container>
  		</UserProvider>
  	</Fragment>
  	)
}

export default MyApp
