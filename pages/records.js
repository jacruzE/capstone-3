import { useState, useEffect } from 'react'
import Head from 'next/head'
import { Row, Col } from 'react-bootstrap'
import { Card, Modal, Form, ListGroup } from 'react-bootstrap'
import { Alert, Button, ButtonGroup } from 'react-bootstrap'
import Banner from '../components/Banner'
import Swal from 'sweetalert2'
import Router from 'next/router'
import moment from 'moment'

export default function Records(){
	const [ search, setSearch ] = useState('')
	const [ type, setType ] = useState('All')

	const [ categories, setCategories ] = useState([])
	const [ catName, setCatName ] = useState('Select')
	const [ catType, setCatType ] = useState('Select')

	const [ recAmount, setRecAmount ] = useState('')
	const [ recName, setRecName ] = useState('')
	const [ cnOption, setCnOption ] = useState([])
	const [ ctOption, setCtOption ] = useState([])

	const [ recordsArray, setRecordsArray ] = useState([])
	const [ recordsStorage, setRecordsStorage ] = useState([])

	const [ showEdit, setShowEdit ] = useState(false)
	const [ nameEdit, setNameEdit ] = useState('')
	const [ typeEdit, setTypeEdit ] = useState('')
	const [ idEdit, setIdEdit ] = useState('')
	const [ catEdit, setCatEdit ] = useState('')
	const [ amountEdit, setAmountEdit ] = useState(0)

	const [ show, setShow ] = useState(false)
	const [ hasNoBlank, setHasNoBlank ] = useState(false)

	const fileRef = React.createRef()
	const [ csvData, setCsvData ] = useState([])
	const [ uploadReady, setUploadReady ] = useState(false)
	const [ CsvAddPending, setCsvAddPending ] = useState(false)
	const [ userBalance, setUserBalance ]= useState(0)

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(type !== 'All'){
				setRecordsArray(data.filter(item => item.transaction.type === type))
				setRecordsStorage(data.map(item=>item))
			} else{
				setRecordsArray(data.map(item => item))
				setRecordsStorage(data.map(item=>item))
			}
		})
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/details`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUserBalance(data.balance.current)
		})
	}, [type, show, CsvAddPending])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategories(data.map(category => category))
		})

		if(catName === 'Select' && catType !== 'Select'){
			setCnOption(categories.filter(category => category.type === catType))
		} else if(catName !== 'Select' && catType === 'Select'){
			let tempCats = categories.filter(category => category.name === catName)
			let tempTypes = []
			tempCats.forEach(cat => {
				if(!tempTypes.find(type => type === cat.type)){
					tempTypes.push(cat.type)
				}
			})
			setCtOption(tempTypes)
		} else if (catName === 'Select' && catType === 'Select'){
			setCnOption(categories.map(category => category))
			let tempTypes = []
			categories.forEach(category => {
				if(!tempTypes.find(type => type === category.type)){
					tempTypes.push(category.type)
				}
			})
			setCtOption(tempTypes)
		} else if(catName !== 'Select' && catType !== 'Select'){
			setCnOption(categories.filter(category => category.type === catType))
			let tempTypes = []
			categories.forEach(category => {
				if(category.type === catType){
					if(!tempTypes.find(type => type === category.type)){
						tempTypes.push(category.type)
					}
				}
			})
			setCtOption(tempTypes)
		}

	}, [show, catType, catName])

	useEffect(() => {
		if(recAmount !== '' && recName !== 'Choose' && catName !== 'Select' && catType !== 'Select'){
			setHasNoBlank(true)
		} else {
			setHasNoBlank(false)
		}
	}, [recAmount, recName, catName, catType])

	const categoryTypeOptions = ctOption.map(
		(option, i) => {
		return (
				<option key={i} value={option}>{option}</option>
			)
	})

	const categoryNameOptions = cnOption.map(option => {
		return(
				<option key={option._id} value={option.name}>{option.name}</option>
			)
	})


	recordsArray.sort((a, b) => new Date(b.transaction.date) - new Date(a.transaction.date))
	const displayRecords = recordsArray.map(record => {
		let color = record.transaction.type === 'Income' ? 'success' : 'danger'
		let operator = record.transaction.type === 'Income' ? '+' : '-'
		let color2 = record.transaction.resultingBalance > 0 ? 'success' : 'danger'
		return(
			<Card key={record._id}>
				<Card.Body>
		 			<Card.Title>
		 				{record.transaction.name}
		 			</Card.Title>
		 			<span className={`text-${color} d-flex flex-row-reverse`}>{operator}{record.transaction.amount}</span>
					<Card.Text className='d-inline'><span className={`text-${color}`}>{record.transaction.type}</span> ({record.transaction.category})</Card.Text>
					<span className={`text-${color2} d-flex flex-row-reverse`}>{record.transaction.resultingBalance}</span>
					<span className={`text-${color}`}></span>
					<Card.Text className='d-inline'>{moment(record.transaction.date).format('MMMM DD, YYYY')}</Card.Text>
				</Card.Body>
				<Card.Footer>
					<Button variant='outline-dark' onClick={()=>handleShowEdit(record._id)}>Edit Record</Button>
				</Card.Footer>
			</Card>
		)
	})
	
	function searchRecords(e){
		e.preventDefault()
			let searchResult =  []
			recordsStorage.forEach(data => {
				console.log(data)
				if((data.transaction.name.toLowerCase() === search.toLowerCase() || data.transaction.category.toLowerCase() === search.toLowerCase()) && (data.transaction.type === type || type === 'All')) {
					searchResult.push(data)
				}
			})
			if(search === ''){
				setRecordsArray(recordsStorage)
				setType('All')
			} else {
				setRecordsArray(searchResult)
			}
			setSearch('')
	}

	function handleShow(){
		setShow(true)
	}

	function handleClose(){
		setShow(false)
		setCatName('Select')
		setCatType('Select')
		setRecAmount('')
		setRecName('')
	}

	function handleShowEdit(recId){
		setIdEdit(recId)
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records/${recId}`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				setRecName(data[0].transaction.name)
				setCatType(data[0].transaction.type)
				setRecAmount(data[0].transaction.amount)
				setCatName(data[0].transaction.category)
				setShowEdit(true)
			}
		})	

	}

	function handleCloseEdit(){
		setShowEdit(false)
		setCatName('Select')
		setCatType('Select')
		setRecAmount('')
		setRecName('')
	}

	function editRecord(e){
		e.preventDefault()
		Swal.fire({
			title: 'Are you sure?',
			showDenyButton: true,
			confirmButtonText:'Save Edit',
			denyButtonText: 'Cancel',
			confirmButtonColor: '#d33',
			denyButtonColor: '#3085d6'
		})
		.then(result => {
			if(result.isConfirmed){
				Swal.fire('Saving...')
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records/${idEdit}`, {
					method: 'PUT',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name: recName,
						type: catType,
						category: catName,
						amount: recAmount,
						recordId: idEdit
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data){
						Swal.fire({icon:'success',title:'Edit Successful'})
					} else {
						Swal.fire({icon:'error',title:'Something went wrong...'})
					}
				})
			} else if (result.isDenied){
				Swal.fire('Cancelled')
			}
		})
	}

	function deleteRecord(e){
		e.preventDefault()
		Swal.fire({
			title: 'Are you sure?',
			showDenyButton: true,
			confirmButtonText:'Delete Record',
			denyButtonText: 'Cancel',
			confirmButtonColor: '#d33',
			denyButtonColor: '#3085d6'
		})
		.then(result => {
			if(result.isConfirmed){
				Swal.fire('Deleting')
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records/${idEdit}`, {
					method: 'DELETE',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						recordId: idEdit
					})
				})
				.then(res => res.json())
				.then(data => {
					data === true ? Swal.fire('Deleted') : Swal.fire('Error Occured')
					setTimeout(() => handleCloseEdit(), 1700)
				})
			} else if (result.isDenied){
				return null
			}
		})
	}

	function addRecord(e){
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: catName,
				type: catType,
				amount: recAmount,
				name: recName,
				date: moment()
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({icon:'success',title:'New Record Added'})	
				setTimeout(() => {handleClose()}, 1000)
			} else {
				Swal.fire({icon:'error',title:'Something went wrong...'})	
			} 
		})
	}

	const toBase64 = (file) => new Promise((resolve, reject) => {
	    const reader = new FileReader()
	    reader.readAsDataURL(file)
	    reader.onload = () => resolve(reader.result)
	    reader.onerror = error => reject(error)
  	})

	function uploadCSV(e){
	   e.preventDefault()
	   toBase64(fileRef.current.files[0])
	    .then(encodedFile => {
	      fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records/csv`, {
	        method: 'POST',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	          csvData: encodedFile
	        })
	      })
	      .then(res => res.json())
	      .then(data => {
	        if(data.jsonArray.length > 0){
	          setCsvData(data.jsonArray)
	          setUploadReady(true)
	        }
	      })
	   })
	}

	function addCsvRecords(){
		let tempCatFromCsv = []
		csvData.forEach(item => {
			if(!tempCatFromCsv.find(cat => cat === {name: item.transactCateg,type: item.transactType} )){
				tempCatFromCsv.push({name:item.transactCateg, type:item.transactType})
			}
		})
		tempCatFromCsv.filter(temp => {
			let filteredArr = []
			fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {
				headers: {
					'Authorization' : `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				data.forEach(cat => {
					if(cat.name !== temp.name && cat.type !== temp.type){
						filteredArr.push({name:temp.name,type:temp.type})
					}
				})
				return filteredArr
			})
		})
		tempCatFromCsv.forEach((item, i, arr) => {
			fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: item.name,
					type: item.type
				})
			})
			.then(res => res.json())
			.then(data => {
				data === true ? console.log(`New category added`) : console.log('Category already exists')
			})	
		})
			setCsvAddPending(true)
			Swal.fire('Upload will run in background')
			let recBool = false
			let tempCsvArray = csvData.sort((a,b) => moment(a.date) - moment(b.date))
			let z = 0
		const uploadRecords = (tempCsvArray, z) => {
			if(z < tempCsvArray.length){
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`, {
						method: 'POST',
						headers: {
							'Content-Type' : 'application/json',
							'Authorization' : `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							category: tempCsvArray[z].transactCateg,
							type: tempCsvArray[z].transactType,
							amount: tempCsvArray[z].transactAmount,
							name: tempCsvArray[z].transactName,
							date: new moment(tempCsvArray[z].date),
						})
					})
				.then(res => res.json())
				.then(data => {
					console.log(z)
					recBool = data
					z++
					uploadRecords(tempCsvArray, z)
				})

			} else {
				console.log(z)
				if(recBool == true){
					setCsvAddPending(false)
					setCsvData([])
					setUploadReady(false)
					Swal.fire('Csv Data Successfully Added')
					.then(next => Router.reload())
				} else {
					Swal.fire('Csv add error')
				}
			}
		}
		uploadRecords(tempCsvArray, z)

	}

	return(
		<>
			<Head>
				<title>Records || Budget Tracker</title>
			</Head>
			<Banner data='Records'/>
			<Card className='mb-1'>
				<Form onSubmit={e=>uploadCSV(e)}>
					<Form.Group>
                    	<Form.Control type='file' ref={fileRef} accept='csv' required>
                    	</Form.Control>
					</Form.Group>
					{
						uploadReady && csvData.length > 0
 						?	
 							!CsvAddPending
 							?	<Button variant='dark' className='btn-block' onClick={addCsvRecords}>Add Csv Data to records</Button>
 							: 	<Button variant='dark' className='btn-block' disabled>Processing Csv Data . . .</Button>
 						: 	<Button type='submit' variant='outline-dark'>Upload</Button>
					}
				</Form>
			</Card>
			{
				recordsArray.length > 0
				? null
				: <Alert variant='info' className='my-1'>You can upload your records CSV
					<Button variant='secondary' className='p-0'onClick={e=>console.log(`{transactType: (Income/Expense), transactName: (customName), transactAmount: (number), transactCateg: (categoryname), date: (Date)}`,)}>console.log CSV Format</Button>
				</Alert>
			}
			<Form onSubmit={e=>searchRecords(e)} className='mt-3'>
				<Form.Row>
					<Button as={Form.Group} size='lg' variant='outline-success' onClick={handleShow}>Add New</Button>
					<Form.Group as={Col} controlId='Search'>
					{ recordsArray.length > 0
						?	<Form.Control type='text' size='lg' placeholder='Search Record' value={search} onChange={e=>setSearch(e.target.value)} />
						: 	<Form.Control type='text' size='lg' placeholder='Search Record' value={search} onChange={e=>setSearch(e.target.value)} disabled/>
					}
					</Form.Group>
					<Form.Group as={Col} controlId='Type'>
					{ recordsArray.length > 0
						?	<Form.Control as='select' size='lg' value={type} onChange={e=>setType(e.target.value)}>
								<option value="All">All</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</Form.Control>
						: 	<Form.Control as='select' size='lg' value={type} onChange={e=>setType(e.target.value)} disabled>
								<option value="All">All</option>
							</Form.Control>
					}
					</Form.Group>
				</Form.Row>
			</Form>
			<Card>
				<Card.Body>
					<Card.Text>User Balance: {userBalance}</Card.Text>
				    <Card.Title>Record entry totals:</Card.Title>
					<Card.Text>Total Entries: 	{recordsStorage.length}</Card.Text>
					<Card.Text>Total Income Entries: 	{recordsStorage.filter(rec => rec.transaction.type === 'Income').length}</Card.Text>
					<Card.Text>Total Expense Entries: 	{recordsStorage.filter(rec => rec.transaction.type === 'Expense').length}</Card.Text>
				</Card.Body>
			</Card>
			{
				displayRecords
			}
			<Modal size='lg' aria-labelledby='contained-modal-title-vcenter' centered show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title id='container-modal-title-vcenter'>New Record</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => addRecord(e)}>
						<Form.Group controlId='categoryType'>
							<Form.Label>Category Type:</Form.Label>
							<Form.Control as='select' value={catType} onChange={e=>setCatType(e.target.value)}>
								<option value="Select">Select Type</option>
								{categoryTypeOptions}
							</Form.Control>
						</Form.Group>
						<Form.Group controlId='categoryName'>
							<Form.Label>Category Name:</Form.Label>
							<Form.Control as='select' value={catName} onChange={e=>setCatName(e.target.value)}>
								<option value="Select">Select Name</option>
								{categoryNameOptions}
							</Form.Control>
						</Form.Group>
						<Form.Group controlId='amount'>
							<Form.Label>Amount:</Form.Label>
							<Form.Control type='number' value={recAmount} onChange={e => setRecAmount(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId='name'>
							<Form.Label>Name:</Form.Label>
							<Form.Control type='text' value={recName} onChange={e => setRecName(e.target.value)} required/>
						</Form.Group>
						{
							hasNoBlank
							?	<Button type='submit' variant='outline-dark'>Add Record</Button>
							: 	<Button variant='outline-secondary' disabled>Add Record</Button>
						}
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='dark' onClick={handleClose}>Close</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={showEdit} onHide={handleCloseEdit}>
				<Modal.Header>
					<Modal.Title>Edit Category</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => editRecord(e)}>
							<Form.Group controlId='categoryType'>
							<Form.Label>Category Type:</Form.Label>
							<Form.Control as='select' value={catType} onChange={e=>setCatType(e.target.value)}>
								<option value="Select">Select Type</option>
								<option value='Income'>Income</option>
								<option value='Expense'>Expense</option>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId='categoryName'>
							<Form.Label>Category Name:</Form.Label>
							<Form.Control as='select' value={catName} onChange={e=>setCatName(e.target.value)}>
								<option value="Select">Select Name</option>
								{categoryNameOptions}
							</Form.Control>
						</Form.Group>
						<Form.Group controlId='amount'>
							<Form.Label>Amount:</Form.Label>
							<Form.Control type='number' value={recAmount} onChange={e => setRecAmount(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId='name'>
							<Form.Label>Name:</Form.Label>
							<Form.Control type='text' value={recName} onChange={e => setRecName(e.target.value)} required/>
						</Form.Group>
						<ButtonGroup>
							<Button type='submit' variant='warning'>Save Edit</Button> 
							<Button variant='secondary'>↔</Button> 
							<Button variant='danger' onClick={e => deleteRecord(e)}>Delete</Button> 
						</ButtonGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='dark' onClick={handleCloseEdit}>Close</Button>
				</Modal.Footer>
			</Modal>
		</>
		)
}