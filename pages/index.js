import Head from 'next/head'
import { Image, Container } from 'react-bootstrap'
import Banner from '../components/Banner'


export default function Home(){


  return(
    <>
      <Head>
        <title>Home || Budget Tracker</title>
      </Head>
      <Banner data='Budget Tracker'/>
    </>
    )
}