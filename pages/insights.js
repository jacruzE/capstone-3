import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import Head from 'next/head'

import ChartMonthly from '../components/ChartMonthly'
import MonthComparison from '../components/monthlyComparison'


export default function Insights(){

	const [ incomeArr, setIncomeArr ] = useState([])
	const [ expenseArr, setExpenseArr ] = useState([])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			let monthlyIncome = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			let monthlyExpense = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			data.forEach(record => {
				const index = moment(record.transaction.date).month()
				if(record.transaction.type === 'Income'){
					monthlyIncome[index] += parseInt(record.transaction.amount)
				} else {
					monthlyExpense[index] += parseInt(record.transaction.amount)
				}
			})
			setIncomeArr(monthlyIncome)
			setExpenseArr(monthlyExpense)
		})
	}, [])


	return(
		<>
			<Head>
				<title>Monthly Insights | Budget Tracker</title>
			</Head>
			<h1 className='text-center my-3'>Monthly Insights</h1>
			<Tabs defaultActiveKey='compare' id='userInsights'>
				<Tab eventKey='compare' title='Monthly Income-Expense Chart'>
					<MonthComparison values1={incomeArr} values2={expenseArr} label1='Income' label2='Expense' />
				</Tab>
				<Tab eventKey='income' title='Monthly Income'>
					<ChartMonthly rawData={incomeArr} label='Amounts in Php' color='green' hover='yellow' />
				</Tab>
				<Tab eventKey='expense' title='Monthly Expense'>
					<ChartMonthly rawData={expenseArr} label='Amounts in Php' color='indianred' hover='red' />
				</Tab>
			</Tabs>
		</>
		)
}