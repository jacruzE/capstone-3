import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'

import Router from 'next/router'
import Head from 'next/head'

import Swal from 'sweetalert2'


export default function register(){
	const [ email, setEmail ] = useState('')
	const [ pw, setPw ] = useState('')
	const [ cpw, setCpw ] = useState('')
	const [ eHolder, setEHolder ] = useState('placeholder') 
	const [ pwHolder, setpwHolder ] = useState('placeholder') 
	const [ cpwHolder, setcpwHolder ] = useState('') 
	const [ isActive, setIsActive ] = useState(false)

	useEffect(() => {
		if((pw !== '' && cpw !== '' && email !== '') && (pw === cpw)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
		email === '' ? setEHolder(`Enter Email (ex. "youremail@mail.com")`) : setEHolder('')
		pw === '' ? setpwHolder('Enter Secure Password') : setpwHolder('')
		pw !== '' && cpw === '' ? setcpwHolder('Re-enter Password to Confirm') : setcpwHolder('')

	}, [cpw, email, pw])

	function register(e){
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({email:email})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false){
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
					},
					body: JSON.stringify({
						email: email,
						password: pw
					})
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire({icon:'success',title:'Account Registered'})
				})
				Router.push('/login')
			} else {
				Swal.fire({icon:'error',title:'Email is already taken'})
			}
		})
	}


	return(
		<>
			<Head>
				<title>Register || Budget Tracker</title>
			</Head>
			<Form onSubmit={e => register(e)}>
				<Form.Group controlId='email'>
					<Form.Label>Email:</Form.Label>
					<Form.Control type='email' placeholder={eHolder} value={email} onChange={e => setEmail(e.target.value)}/>
				</Form.Group>
				<Form.Group controlId='password'>
					<Form.Label>Password:</Form.Label>
					<Form.Control type='password' placeholder={pwHolder} value={pw} onChange={e => setPw(e.target.value)}/>
				</Form.Group>
				<Form.Group controlId='confirmPassword'>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type='password' placeholder={cpwHolder} value={cpw} onChange={e => setCpw(e.target.value)}/>
				</Form.Group>
				{
					isActive
					?	<Button variant='dark' type='submit'>Register</Button>
					: 	<Button variant='secondary' disabled>Register</Button>
				}
			</Form>
		</>
		)
}