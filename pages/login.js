import { useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login'
import Head from 'next/head'
import Swal from 'sweetalert2'
import Banner from '../components/Banner'

export default function Login(){
	const { setUser } = useContext(UserContext)
	const [ email, setEmail ] = useState('')
	const [ password, setPassword ] = useState('')

	function authenticate(e){
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.error && data.error == 'popup_closed_by_user'){
				return null
			} else {
				if(data.access){
					localStorage.setItem('token', data.access)
					fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/details`, {
						headers: {
							'Authorization' : `Bearer ${data.access}`
						}
					})
					.then(res => res.json(0))
					.then(data => {
						setUser({id:data._id,email:email})
						Swal.fire({icon:'success',title:'Login Successful'})
						setTimeout(()=> {Router.push('/categories')}, 1000)
					})
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Login failed'
					})
				}
			}
		})
	}

	const authGoogleToken = (res) => {
		const payload = {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				tokenId: res.tokenId,
				googleToken: res.accessToken
			})
		}
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/verify-google-id-token`, payload)
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/details`, {
					headers: {
						'Authorization' : `Bearer ${data.access}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({id:data._id,email:data.email})
					Swal.fire({icon:'success',title:'Login Successful'})
					setTimeout(()=> Router.push('/categories'), 1000)
				})
			} else {
				if(data.error === 'google-auth-error'){
					Swal.fire({icon:'error',title:'Auth Error',text:'Google Authentication Failed'})
				} else if(data.error === 'login-type-error'){
					Swal.fire({icon:'error',title:'Login Error',text:'Try alternative login procedures.'})
				}
			}
		})
	}

	return(
		<>
			<Head>
				<title>Login || Budget Tracker</title>
			</Head>
			<Banner data='Login'/>
			<Form onSubmit={(e) => authenticate(e)}>
				<Form.Group controlId='email'>
					<Form.Label>Email</Form.Label>
					<Form.Control type='text' placeholder='enter email address' value={email} onChange={(e) => setEmail(e.target.value)}  required/>
				</Form.Group>
				<Form.Group controlId='password'>
					<Form.Label>Password</Form.Label>
					<Form.Control type='password' placeholder='enter password' value={password} onChange={(e) => setPassword(e.target.value)}  required/>
				</Form.Group>
				<Button variant='dark' type='submit'>Login</Button>
				<GoogleLogin
					clientId="593398040925-918dk7v0dkg568frs82021798se6sthi.apps.googleusercontent.com"
					buttonText="Login via Google"
					color="green"
					className="w-100 text-center d-flex justify-content-center"
					cookiePolicy={'single_host_origin'}
					onSuccess={authGoogleToken}
					onFailure={authGoogleToken}
				/>
			</Form>
		</>
		)
}