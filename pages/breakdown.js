import { useEffect, useState } from 'react'
import Head from 'next/head'
import { Form, Col, Table } from 'react-bootstrap'
import PieBreakdown from '../components/PieBreakdown'
import moment from 'moment'

export default function Breakdown(){

	const [ base, setBase ] = useState('')
	const [ range, setRange ] = useState('')

	const [ recordsData, setRecordsData ] = useState([])
	const [ categoriesData, setCategoriesData ] = useState([])
	const [ records, setRecords ] = useState([])
	const [ categories, setCategories ] = useState([])


	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/records`,{
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setRecordsData(data.map(data=>data))
		})

		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/categories`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategoriesData(data.map(data=>data))
		})
		setTimeout(() => {
			setBase(new moment().date(1).format('YYYY-MM-DD'))
			setRange(new moment().date(0).add(1,'months').format('YYYY-MM-DD'))
		}, 500)
	}, [])

	useEffect(() => {
		let temp = []
		let tempcats = []
		categoriesData.forEach(data=> {
			if(!tempcats.find(temp => temp == {name:data.name,type:data.type})){
				tempcats.push({name:data.name,type:data.type})
			}
		})
		setCategories(tempcats.map(cat => {
			temp.push(0)
			 return `${cat.name}, (${cat.type})`
		}))
		recordsData.sort((a,b) => moment(a.transaction.date) - moment(b.transaction.date))
		recordsData.forEach(data => {
			if(moment(data.transaction.date).isSameOrAfter(base) && moment(data.transaction.date).isSameOrBefore(range) ){
				const index = categories.findIndex((category, i) => category.includes(data.transaction.category) )
				temp[index] += parseInt(data.transaction.amount)
			}
		})
		setRecords(temp)
	}, [range, base])


	return(
		<>
			<Head>
				<title>Trends | Budget Tracker</title>
			</Head>
			<h1 className="my-3">Balance Trend</h1>
			<Form className='d-flex'>
				<Form.Group as={Col} sm={6}>
					<Form.Label>From:</Form.Label>
					<Form.Control type='date' value={base} onChange={e => setBase(e.target.value)}/>
				</Form.Group>
				<Form.Group as={Col} sm={6}>
					<Form.Label>To:</Form.Label>
					<Form.Control type='date' value={range} onChange={e => setRange(e.target.value)}/>
				</Form.Group>
			</Form>
			<PieBreakdown records={records} categories={categories} />
		</>
		)
}