import { useContext, useState, useEffect } from 'react'
import UserContext from '../UserContext'
import { Form, Button } from 'react-bootstrap'
import Head from 'next/head'
import Swal from 'sweetalert2'
import Router from 'next/router'

export default function changePass(){
	const { user, unsetUser } = useContext(UserContext)

	const [ opw, setOpw ] = useState('')
	const [ pw, setPw ] = useState('')
	const [ cpw, setCpw ] = useState('')

	const changePassword = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URI}/api/users/changePassword`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				password: opw,
				newPassword: pw
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data == true){
				Swal.fire('Password changed. Logging out...')
				setTimeout(() =>{
					unsetUser()
					Router.push('/login')
				}, 600)
			} else {
				Swal.fire('Failed')
			}
		})
	}


	return (
		<>
			<Head>
				<title>Change Password</title>
			</Head>
			<h1 className="text-center">Change Password</h1>
			<Form onSubmit={e=>changePassword(e)}>
				<Form.Group>
					<Form.Label>Enter current:</Form.Label>
					<Form.Control type='password' value={opw} onChange={e => setOpw(e.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Enter new password:</Form.Label>
					<Form.Control type='password' value={pw} onChange={e => setPw(e.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm new password:</Form.Label>
					<Form.Control type='password' value={cpw} onChange={e => setCpw(e.target.value)}/>
				</Form.Group>
				{
					user.id && opw !== '' && cpw !== '' && cpw === pw
					?	<Button type='submit' variant='info'>Change Password</Button>
					: 	<Button variant='secondary' disabled>Change Password</Button>
				}
			</Form>
		</>
		)
}