import { Line } from 'react-chartjs-2'


export default function Trand({ values1, values2, label1, label2 }){

	const data = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		datasets: [
			{
				label: label1,
				backgroundColor: 'lightblue',
				borderColor: 'black',
				borderWidth: 1,
				hoverBackgroundColor: 'black',
				data:  values1
			},
			{
				label: label2,
				backgroundColor: 'indianred',
				borderColor: 'black',
				borderWidth: 1,
				hoverBackgroundColor: 'black',
				data:  values2
			},
			
		]
	}


	return(
			<Line data={ data } />
		)
}