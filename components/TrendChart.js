import { Line } from 'react-chartjs-2'


export default function Trand({ values, dates }){

	const data = {
		labels: dates,
		datasets: [{
			label: 'Balance',
			backgroundColor: 'lightblue',
			borderColor: 'black',
			borderWidth: 1,
			hoverBackgroundColor: 'black',
			data:  values
		}]
	}


	return(
			<Line data={ data } />
		)
}