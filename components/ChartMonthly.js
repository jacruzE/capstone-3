import { Bar } from 'react-chartjs-2'



export default function BarChart({ rawData, label, color, hover }){

	const data = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		datasets: [{
			label: label,
			backgroundColor: color,
			borderColor: 'black',
			borderWidth: 1.5,
			hoverBackgroundColor: hover,
			data:  rawData
		}]
	}

	return(
			<Bar data={data} />
		)
}