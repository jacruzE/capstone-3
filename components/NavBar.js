import { Navbar, Nav } from 'react-bootstrap'

import { useContext } from 'react'
import Link from 'next/link'

import UserContext from '../UserContext'


export default function NavBar(){

	const { user } = useContext(UserContext)

	return(
		<Navbar bg='dark' expand='lg' className='navbar-dark'>
			<Link href='/'>
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls='basic-navbar-nav' />
			<Navbar.Collapse>
				{
					!user.id
					?	<>
						<Nav className="ml-auto mr-3">
							<Link href='/login'>
								<a className="nav-link">Login</a>
							</Link>
							<Link href='/register'>
								<a className="nav-link">Register</a>
							</Link>
						</Nav>
						</>
					:	<>
						<Nav className="mr-auto">
							<Link href='/categories'>
								<a className="nav-link">Categories</a>
							</Link>
							<Link href='/records'>
								<a className="nav-link">Records</a>
							</Link>
							<Link href='/insights'>
								<a className="nav-link">Insights</a>
							</Link>
							<Link href='/trends'>
								<a className="nav-link">Trends</a>
							</Link>
							<Link href='/breakdown'>
								<a className="nav-link">Breakdown</a>
							</Link>
						</Nav>
						<Nav className='mr-5'>
							<Link href='/changepass'>
								<a className="nav-link">{user.email} (change password)</a>
							</Link>
							<Link href='/logout'>
								<a className="nav-link">Logout</a>
							</Link>
						</Nav>
						</>
				}
			</Navbar.Collapse>
		</Navbar>
		)
}