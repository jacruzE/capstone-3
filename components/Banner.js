import { Row, Col } from 'react-bootstrap'
import { Jumbotron } from 'react-bootstrap'


export default function Banner({data}){

	return(
			<Row>
				<Col>
					<Jumbotron>
						<h1>{data}</h1>
					</Jumbotron>
				</Col>
			</Row>
		)
}