import { Pie } from 'react-chartjs-2'
import { useState, useEffect } from 'react'
import { colorRandomizer } from '../helpers'

export default function PieBreakdown({records, categories}){

	const [ colors, setColors ] = useState([])
	const [ hovers, setHovers ] = useState([])
	console.log(categories, colors, records)
	useEffect(() => {
		setColors(categories.map(() => `#${colorRandomizer()}`))
		setHovers(categories.map(() => `#${colorRandomizer()}`))
	}, [categories])

	const data = {
		labels: categories,
		datasets: [{
			label: 'PieBreakdown',
			backgroundColor: colors,
			borderColor: 'black',
			borderWidth: 1,
			hoverBackgroundColor: colors,
			data: records
		}]
	}

	return(
			<Pie data={data} />
		)
}